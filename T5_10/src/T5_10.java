import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_10 {

	/**
	10) Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, despu�s nos
	pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. Al final
	mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String strnro = JOptionPane.showInputDialog("Introduce un n�mero de ventas a introducir");
		int veces = Integer.parseInt(strnro);
		double suma = 0;
		int vez = 0;
		while (vez<veces) {
			String sr = JOptionPane.showInputDialog("Introduce la venta " + (vez+1));
			double r = Double.parseDouble(sr);
			suma += r;
			vez++;
		}	
		JOptionPane.showMessageDialog(null, "Ventas: " + suma);		
	}

}
