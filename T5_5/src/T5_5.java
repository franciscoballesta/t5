import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_5 {

	/**
	5) Lee un n�mero por teclado e indica si es divisible entre 2 (resto = 0). Si no lo es, tambi�n
	debemos indicarlo.	 
	*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String sr = JOptionPane.showInputDialog("Introduce un n�mero");
		int n = Integer.parseInt(sr);
		int r = n % 2;
		if(r==0) {
			JOptionPane.showMessageDialog(null, "Par");		
		}else {
			JOptionPane.showMessageDialog(null, "ImPar");		
		}
	}

}
