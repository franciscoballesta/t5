/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_9 {

	/**
	9) Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle
	que desees.	
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (int i = 1; i <= 100; i++) {
			if(i % 2 == 0) {
				System.out.println("Divisible por 2: " + i);	
			}
			if(i % 3 == 0){
				System.out.println("Divisible por 3: " + i);	
			}
			
		}

	}

}
