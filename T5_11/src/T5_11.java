import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_11 {

	/**
	11) Crea una aplicaci�n que nos pida un d�a de la semana y que nos diga si es un d�a laboral
	o no. Usa un switch para ello.	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String strnro = JOptionPane.showInputDialog("Introduce dia de la semana");
		int dia = Integer.parseInt(strnro);
		switch (dia) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			JOptionPane.showMessageDialog(null, "Laboral: " + dia);		

			break;
			
		case 6:
		case 7:
			JOptionPane.showMessageDialog(null, "Festivo: " + dia);		
			
			break;

		default:
			JOptionPane.showMessageDialog(null, "No es v�lido: " + dia);		
			
			break;
		}

	}

}
