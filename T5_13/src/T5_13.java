import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_13 {

	/**
	13) Crea una aplicaci�n llamada CalculadoraInversa, nos pedir� 2 operandos (int) y un signo
	aritm�tico (String), seg�n este �ltimo se realizara la operaci�n correspondiente. Al final
	mostrara el resultado en un cuadro de dialogo.
	Los signos aritm�ticos disponibles son:
	+: suma los dos operandos.
	-: resta los operandos.
	*: multiplica los operandos.
	/: divide los operandos, este debe dar un resultado con decimales (double)
	^: 1� operando como base y 2� como exponente.
	%: m�dulo, resto de la divisi�n entre operando1 y operando2.	 
	*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String strnro = JOptionPane.showInputDialog("Introduce primer operador");
		int op1 = Integer.parseInt(strnro);
		strnro = JOptionPane.showInputDialog("Introduce segundo operador");
		int op2 = Integer.parseInt(strnro);
		strnro = JOptionPane.showInputDialog("Introduce operador (+-/*^%)");
		double res = 0;
		switch (strnro) {
		case "+":
			res = op1 + op2;
			break;
		case "-":
			res = op1 - op2;
			break;
		case "*":
			res = op1 * op2;
			break;
		case "/":
			res = op1 / op2;
			break;
		case "^":
			res = op1 ^ op2;
			break;
		case "%":
			res = op1 % op2;
			break;

		default:
			break;
		}
		JOptionPane.showMessageDialog(null, "Resultado: " + res);
		
	}

}
