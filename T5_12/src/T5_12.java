import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_12 {

	/**
	12) Escribe una aplicaci�n con un String que contenga una contrase�a cualquiera. Despu�s
	se te pedir� que introduzcas la contrase�a, con 3 intentos. Cuando aciertes ya no pedir� mas
	la contrase�a y mostrara un mensaje diciendo �Enhorabuena�. Piensa bien en la condici�n
	de salida (3 intentos y si acierta sale, aunque le queden intentos)	 
	*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String password = "123";
		int veces = 0;
		boolean correcto = false;
		do {
			String pass = JOptionPane.showInputDialog("Introduce password");
			veces++;
			if (pass.equals(password)) {
				correcto=true;
			}
		} while (veces <3 && !correcto);
		if(correcto) 
			JOptionPane.showMessageDialog(null, "Correcto");
		else 
			JOptionPane.showMessageDialog(null, "Incorrecto");
				
	}

}
