import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_4 {

	/**
	4) Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2
	). El radio se pedir� por teclado
	(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el
	m�todo pow de Math.	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String sr = JOptionPane.showInputDialog("Introduce el radio");
		double r = Double.parseDouble(sr);
		final double PI = 3.1416;
		double area = PI * (Math.pow(r, 2));
		JOptionPane.showMessageDialog(null, "Area= " + area);		
	}

}
