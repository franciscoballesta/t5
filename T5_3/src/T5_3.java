/**
 * 
 */

/**
 * @author francis
 *
 */
import javax.swing.JOptionPane;
public class T5_3 {

	/**
	3) Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir
	(recuerda usar JOptionPane).
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String n = JOptionPane.showInputDialog("Introduce tu nombre");
		JOptionPane.showMessageDialog(null, "Bienvenido " + n);

	}

}
