import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author francis
 *
 */
public class T5_6 {

	/**
	6) Lee un n�mero por teclado que pida el precio de un producto (puede tener decimales) y
	calcule el precio final con IVA. El IVA sera una constante que sera del 21%	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final double IVA = 1.21;
		String sr = JOptionPane.showInputDialog("Introduce un precio");
		double p = Double.parseDouble(sr);
		JOptionPane.showMessageDialog(null, "Importe = " + (p*IVA));		
		
	}

}
